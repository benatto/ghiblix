from app import app

from tests.test_data import FAKE_MOVIES
from tests.test_data import FAKE_PEOPLE

from mock import patch


app.testing = True
app.debug = True


class TestMoviesResource(object):
    @patch("project.common.ghibli.get_people", return_value=FAKE_PEOPLE)
    @patch("project.common.ghibli.get_movies", return_value=FAKE_MOVIES)
    def test_if_movies_endpoint_handles_200(self, mock_movies, mock_get_peole):
        # Act
        response = app.test_client().get("/movies")

        # Assert
        assert response.status_code == 200

    @patch("project.common.ghibli.get_people", side_effect=[Exception])
    @patch("project.common.ghibli.get_movies", return_value=FAKE_MOVIES)
    def test_if_movies_endpoint_handles_500(self, mock_movies, mock_get_peole):
        # Act
        response = app.test_client().get("/movies")

        # Assert
        assert response.status_code == 500

    def test_if_movies_endpoint_handles_404(self):
        # Act
        response = app.test_client().get("/invalid-endpoint")

        # Assert
        assert response.status_code == 404

    def test_if_movies_endpoint_handles_405(self):
        # Act
        response = app.test_client().post("/movies")

        # Assert
        assert response.status_code == 405
