from project.services import movies_service
from tests.test_data import FAKE_MOVIES
from tests.test_data import FAKE_PEOPLE

from mock import patch


class TestMoviesService(object):
    @patch("project.common.ghibli.get_people", return_value=FAKE_PEOPLE[:-1])
    @patch("project.common.ghibli.get_movies", return_value=FAKE_MOVIES)
    def test_should_return_all_movies_and_people(self, mock_movies, mock_people):
        # Arrange
        movie_princess_mononoke = "0440483e-ca0e-4120-8c50-4c8cd9b965d6"
        movie_tales_from_earthsea = "112c1e67-726f-40b1-ac17-6974127bb9b9"
        movie_ponyo = "758bf02e-3122-46e0-884e-67cf83df1786"
        movie_the_wind_rises = "67405111-37a5-438f-81cc-4666af60c800"

        # Act
        movies = movies_service.get_all()

        # Assert
        assert len(movies.get(movie_princess_mononoke).get("people")) == 4
        assert len(movies.get(movie_tales_from_earthsea).get("people")) == 1
        assert len(movies.get(movie_ponyo).get("people")) == 1
        assert len(movies.get(movie_the_wind_rises).get("people")) == 2

    @patch("project.common.logger.logger.error")
    @patch("project.common.ghibli.get_people", return_value=[FAKE_PEOPLE[-1]])
    @patch("project.common.ghibli.get_movies", return_value=FAKE_MOVIES)
    def test_should_handle_data_with_bad_format(
        self, mock_movies, mock_people, mock_logger
    ):
        # Act
        movies = movies_service.get_all()

        # Assert
        assert mock_logger.call_count == 2
        assert len(movies.keys()) == 20
