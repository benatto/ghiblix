FROM python:3

LABEL name=ghibli-service

LABEL maintainer="benatto@gmail.com"

WORKDIR /app

COPY requirements.txt ./
RUN pip3 install -r requirements.txt

COPY . /app

CMD ["python", "app.py"]
