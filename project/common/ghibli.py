from datetime import timedelta
from typing import Any
from typing import Dict

from project.common.constants import GHIBLI_BASE_URL
from project.common.constants import GHIBLI_MOVIES_ENDPOINT
from project.common.constants import GHIBLI_PEOPLE_ENDPOINT
from project.common.logger import logger

import requests
import requests_cache


expire_after = timedelta(minutes=1)
requests_cache.install_cache("movies", expire_after=expire_after)


def _make_request(endpoint: str) -> Dict[str, Any]:
    try:
        res = requests.get(endpoint)
        return res.json()
    except Exception as err:
        logger.error(f"Error fetching data: {str(err)}")
        return {}


def get_movies() -> Dict[str, Any]:
    movies_endpoint = f"{GHIBLI_BASE_URL}{GHIBLI_MOVIES_ENDPOINT}"
    return _make_request(movies_endpoint)


def get_people() -> Dict[str, Any]:
    people_endpoint = f"{GHIBLI_BASE_URL}{GHIBLI_PEOPLE_ENDPOINT}"
    return _make_request(people_endpoint)
