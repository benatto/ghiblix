GHIBLI_BASE_URL = "https://ghibliapi.herokuapp.com"
GHIBLI_MOVIES_ENDPOINT = "/films"
GHIBLI_PEOPLE_ENDPOINT = "/people"

TEMPLATE_200 = "movies.html"
TEMPLATE_500 = "error_500.html"
