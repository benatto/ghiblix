from typing import Any
from typing import Dict

from project.common import ghibli
from project.common.logger import logger


def _group_movies_by_id() -> Dict[str, Any]:
    response = {}

    movies = ghibli.get_movies()
    for movie in movies:
        try:
            movie_id = movie["id"]
            response[movie_id] = movie
            response[movie_id]["people"] = []
        except Exception as err:
            logger.error(f"Error adding new movie: {str(err)}")
            continue

    return response


def _add_people_to_movies(movies: Dict[str, Any]) -> None:
    people = ghibli.get_people()
    for p in people:
        for movie in p.get("films"):
            try:
                movie_id = movie.split("/")[-1]
                name = p.get("name")
                movies[movie_id]["people"].append(name)
            except Exception as err:
                logger.error(f"Error adding new character: {str(err)}")
                continue


def get_all() -> Dict[str, Any]:
    """
    Get all returns movies with people that appeared in the movie

    Returns:
        movies (dict): All movies with respecitive people
    """

    movies = _group_movies_by_id()
    _add_people_to_movies(movies)

    return movies
