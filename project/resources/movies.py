from project.common.constants import TEMPLATE_200
from project.common.constants import TEMPLATE_500
from project.services import movies_service

from flask import make_response
from flask import render_template
from flask_restful import Resource


class Movies(Resource):
    def get(self):
        headers = {"Content-Type": "text/html"}
        try:
            data = movies_service.get_all()
            return make_response(
                render_template(TEMPLATE_200, movies=data), 200, headers
            )
        except Exception:
            return make_response(render_template(TEMPLATE_500, movies={}), 500, headers)
