from project.resources.movies import Movies


def add(api):
    # Add all endpoints here
    api.add_resource(Movies, "/movies")
