clean:
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +
	find . -name '.pytest_cache' -exec rm -fr {} +
	find . -name '.mypy_cache' -exec rm -fr {} +

help:
	@echo "Helper to build and run the project"
	@echo "- clean: clean py object files"
	@echo "- test: runs all integration tests with docker"
	@echo "- lint: runs a code linter"
	@echo "- build up: runs the code"

lint:
	docker-compose -f docker-compose.yml run ghibli flake8 --ignore=E501,E266 src tests

test:
	docker-compose -f docker-compose.testing.yml run ghibli python3 -m pytest --cov-report term-missing --cov=/app/project/ -s -v tests/

mypy:
	docker-compose -f docker-compose.yml run ghibli mypy --ignore-missing-imports /app/project/services /app/project/resources /app/project/common

build:
	docker-compose build

up:
	docker-compose up