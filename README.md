# Ghiblix

Your Ghibli Video Streaming Service :smile:

You can find the app running at heroku: https://glacial-mesa-98073.herokuapp.com/movies

Local: http://localhost:8000/movies

## Build and Run

I enjoy using make to automate commands, builds and make my life easier. I'm using
`docker` to build and run the project.

```
$ make build up
```

## Run Tests

For tests I'm using `pytest`.

```
$ make test

----------- coverage: platform linux, python 3.8.5-final-0 -----------
Name                                 Stmts   Miss  Cover   Missing
------------------------------------------------------------------
project/common/constants.py              5      0   100%
project/common/helpers.py               24     10    58%   19-24, 28-29, 33-34
project/common/logger.py                 8      0   100%
project/resources/movies.py             15      0   100%
project/services/movies_service.py      31      0   100%
------------------------------------------------------------------
TOTAL                                   83     10    88%
```

## Run Linter

For linter I'm using `flake8`.

```
$ make lint
```

## Run Mypy

I like the idea of typing hint in a dynamic language, it helps you to catch bugs in early stage.

```
$ make mypy
```

## Space for Improvement

This is a code challenge, so I tried to be more objective and direct, but I can see some
points where I could improve.

- *Cache*: I'm using cache on top of requests, it solves my problem that I have at this moment.
- *Naming Convention*: I'm terrible naming variables :).
- *Logging*: We could redirect logs to StackDiver or any other logging solution.
- *Error Handling*: Be less generic and more specific when handling errors and maybe send errors to Sentry.

# Code Structure

Developers they have different approach, this is normal. So I will try to explain a little bit here
my approach.

- *project.common*: usualy I add in common all resources that will be used in the whole project.
- *project.resources*: The handlers, the endpoints handler (HTTP).
- *project.services*: To not add the business logic inside of a handler (HTTP) I have created the services.
It might look as overkill now, but if the project scales it will be a good solution.
- *routes*: The place to add all routes.
- *app*: Entry Point.
- *Code Style*: I use tools to help me with this problem: `black` and `flake8`.

## License

```
/*
 * "THE BARBECUE-WARE LICENSE" (Revision 1):
 *
 * <benatto@gmail.com> wrote this code. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can make me a brazilian barbecue, including beers
 * and caipirinha in return to Paulo Leonardo Benatto.
 *
 * The quality of the barbecue depends on the amount of beer that has been
 * purchased.
 */
```
