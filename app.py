import os

from project.resources.movies import Movies

import routes

from flask import Flask
from flask import jsonify
from flask_restful import Api


def create_app(config=None):
    app = Flask(__name__, template_folder="templates")
    app.config.from_object(config)

    return app


app = create_app()
api = Api(app)
routes.add(api)


@app.errorhandler(404)
def resource_not_found(e):
    return jsonify(error=str(e)), 404


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000, debug=True)
